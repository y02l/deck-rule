# デッキのルールに関するサービス

## APIリファレンス
### デッキルール
#### HTTPリクエスト
* GET http://deck-rule/rules/
* POST http://deck-rule/rules/
* PATCH http://deck-rule/rules/{id}
* DELETE http://deck-rule/rules/{id}

* POST http://deck-rule/rules/all

#### フォーマット
```
[
  {
    _id: str,             auto allocate, can not modify
    name: str,            default: ""
    cost_capacity: bool,  default: true
    max_capacity: int,    default: 10
    costs: [              default: []
      {
        name: str
        cost: int
      },
      ...
    ]
  },
  ...
]
```
