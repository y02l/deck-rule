from flask import Flask, request, jsonify
from flask_pymongo import PyMongo

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
app.config['MONGO_URI'] = 'mongodb://db:27017/deck_rule'
mongo = PyMongo(app)

rules = mongo.db.rules


@app.route('/rules', methods=['GET'])
def get_rules():
    res = list(rules.find())
    return jsonify(res), 200


@app.route('/rules', methods=['POST'])
def post_rules():
    req = dict(name='default name',
               gen02=True,
               highlander=True,
               uni_monster_type=False,
               cost_capacity=True,
               max_capacity=10,
               costs=[])
    req.update(request.json)
    _id = rules.insert_one(req).inserted_id

    convdata = rules.find_one({'_id': _id})
    convdata['_id'] = str(_id)
    rules.delete_one({'_id': _id})
    _id = rules.insert_one(convdata).inserted_id

    res = rules.find_one({'_id': _id})
    return jsonify(res), 200


@app.route('/rules/<string:_id>', methods=['PATCH'])
def patch_rules(_id):
    req = request.json
    rules.update_one({'_id': _id}, {'$set': req}, upsert=False)
    res = rules.find_one({'_id': _id})
    return jsonify(res), 200


@app.route('/rules/<string:_id>', methods=['DELETE'])
def delete_rules(_id):
    res = rules.find_one({'_id': _id})
    rules.delete_one({'_id': _id})
    return jsonify(res), 200


@app.route('/rules/all', methods=['POST'])
def post_rules_all():
    init = dict(name='default name',
                gen02=True,
                highlander=True,
                uni_monster_type=False,
                cost_capacity=True,
                max_capacity=10,
                costs=[])
    keys = [
        'name', 'gen02', 'highlander', 'uni_monster_type', 'cost_capacity',
        'max_capacity', 'costs'
    ]
    rules.delete_many({})
    req = request.json
    for req_rule in req:
        req_rule = dict(init, **req_rule)
        newdata = {k: req_rule[k] for k in keys}
        _id = rules.insert_one(newdata).inserted_id

        convdata = rules.find_one({'_id': _id})
        convdata['_id'] = str(_id)
        rules.delete_one({'_id': _id})
        _id = rules.insert_one(convdata).inserted_id
    return 'done', 200
