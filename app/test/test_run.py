import json

import pytest

from run import app, mongo


@pytest.fixture(scope='function', autouse=True)
def prepost():
    app.config['TESTING'] = True
    mongo.db.drop_collection("rules")
    yield
    pass


def get_rules(client):
    resp = client.get('/rules', content_type='application/json')
    return resp


def post_rules(client, name):
    resp = client.post('/rules',
                       data=json.dumps(dict(name=name)),
                       content_type='application/json')
    return resp, resp.get_json()['_id']


def patch_rules(client, _id, dic):
    resp = client.patch('/rules/' + _id,
                        data=json.dumps(dic),
                        content_type='application/json')
    return resp


def delete_rules(client, _id):
    resp = client.delete('/rules/' + _id, content_type='application/json')
    return resp


def test_post_rules():
    client = app.test_client()
    resp, _ = post_rules(client, 'aa')
    assert resp.status_code == 200


def test_get_rules():
    client = app.test_client()
    _, _id = post_rules(client, 'aa')
    resp = get_rules(client)
    res = {d['_id']: d for d in resp.get_json()}
    assert res[_id]['name'] == 'aa'


def test_patch_rules():
    client = app.test_client()
    _, _id = post_rules(client, 'aa')
    patch_rules(client, _id, dict(name='cc'))
    resp = get_rules(client)
    res = {d['_id']: d for d in resp.get_json()}
    assert res[_id]['name'] == 'cc'


def test_delete_rules():
    client = app.test_client()
    post_rules(client, 'aa')
    _, _id = post_rules(client, 'bb')
    assert len(get_rules(client).get_json()) == 2
    delete_rules(client, _id)
    assert len(get_rules(client).get_json()) == 1


def test_post_rules_all():
    client = app.test_client()
    post_rules(client, 'aa')
    post_rules(client, 'bb')
    resp = client.post('/rules/all',
                       data=json.dumps([dict(name= 'a')]),
                       content_type='application/json')
    assert len(get_rules(client).get_json()) == 1
    resp = client.post('/rules/all',
                       data=json.dumps([dict(name= 'rr'), dict(name='jj')]),
                       content_type='application/json')
    assert len(get_rules(client).get_json()) == 2
